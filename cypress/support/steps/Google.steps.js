import { googlePage } from '../pages';

Given(/^que abro o link da página do google$/, () => {
	googlePage.open();
});

When(/^mover o mouse para o icone de teclado$/, () => {
	googlePage.abrirTooltip();
});

Then(/^deve ser apresentado seu tooltip com uma descrição$/, () => {
	googlePage.validarTooltip();
});

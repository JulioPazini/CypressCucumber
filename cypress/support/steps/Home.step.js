/* global Given, Then, When */
import { homePage } from '../pages';

Given(/^que abro o link da página$/, () => {
  homePage.open();
});

Then(/^a página deve ser aberta e apresentar o título$/, () => {
  homePage.validarPagina();
});

Then(/^deve ser selecionado o texto do título$/, () => {
	homePage.selecionarTexto();
});


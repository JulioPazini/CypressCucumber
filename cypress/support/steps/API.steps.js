import { apiPage } from "../pages";

When(/^clico na aba API$/, () => {
  apiPage.clicarAPI();
});

Then(/^a página API deve ser aberta e apresentar o título$/, () => {
  apiPage.validarApiAberta();
});

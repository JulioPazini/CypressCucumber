/// <reference types="Cypress" />

const url = Cypress.config('baseUrl');

class GooglePage {
  open() {
    cy.visit(url);
  }

  abrirTooltip() {
    cy.get('[class="Umvnrc"]').trigger('mouseover');
  }

  validarTooltip() {
    cy.get('[id="app"]').find('Ferramentas de inserção de texto').should('contain', 'Ferramentas de inserção de texto');
  }
}

export const googlePage = new GooglePage();

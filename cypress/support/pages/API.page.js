/// <reference types="Cypress" />

import { homeElements } from '../elements';

class ApiPage {
  clicarAPI() {
    cy.get(homeElements.abaAPI()).first().click();
  }

  validarApiAberta() {
    cy.get(homeElements.mainTitlePage()).should('contain', 'Table of Contents');
  }
}

export const apiPage = new ApiPage();

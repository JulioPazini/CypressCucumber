/// <reference types="Cypress" />

import { homeElements } from "../elements/Home.elements";

const url = Cypress.config('baseUrl');

class HomePage {
  open() {
    cy.visit(url);
  }

  validarPagina() {
    cy.get(homeElements.mainTitlePage()).should('contain', 'Why Cypress?');
  }

  selecionarTexto() {
    cy.get(homeElements.mainTitlePage()).type('{selectAll}');
  }
}

export const homePage = new HomePage();

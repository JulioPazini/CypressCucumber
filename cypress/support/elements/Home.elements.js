class HomeElements {
    mainTitlePage = () => '[class="main-content-title"]';

    abaAPI = () => '[href="/api/table-of-contents"]';
}

export const homeElements = new HomeElements();
let _connection;

const DbPluginUtil = {
  async CriarConexao() {
    const configdb = require('../config/database');
    const oracledb = require('oracledb');
    oracledb.outFormat = oracledb.OUT_FORMAT_OBJECT;
    oracledb.autoCommit = true;

    // Caso não tenha o oracle x64 instalado na máquina, é preciso baixar o instantclient_19_6 e colocar no disco C:
    if (process.platform === 'win32') {
      oracledb.initOracleClient({ libDir: `C://instantclient_19_6` });
    } else {
      oracledb.initOracleClient({ libDir: process.env.LD_LIBRARY_PATH });
    }
    _connection = await oracledb.getConnection(configdb);
    return null;
  },

  async TestarConexao() {
    if (!_connection) {
      await this.CriarConexao();
    }
  },

  async Consultar(statement) {
    await this.TestarConexao();
    try {
      statement = statement.replace(/\n/g, ' ').replace(/\r/g, ' ').replace(';', '');
      const data = await _connection.execute(statement);
      return data;
    } catch (e) {
      throw new Error('failed to execute: ' + statement + '\n' + e.message);
    }
  },
};

Object.freeze(DbPluginUtil);

function loadDBPlugin() {
  return {
    async Consultar({ statement }) {
      return await DbPluginUtil.Consultar(statement);
    },
  };
}

module.exports = {
  loadDBPlugin,
  DbPluginUtil,
};

/// <reference types="cypress" />

const allureWriter = require('@shelex/cypress-allure-plugin/writer');
const cucumber = require('cypress-cucumber-preprocessor').default;
const { loadDBPlugin } = require('../plugins/dbPlugin');

/**
 * @type {Cypress.PluginConfig}
 */
module.exports = (on, config) => {
  on('task', loadDBPlugin());
  on('file:preprocessor', cucumber());
  allureWriter(on, config);
  on('before:browser:launch', (browser = {}, launchOptions) => {
    if (browser.name === 'chrome') {
      launchOptions.args.push('--lang=pt_BR');
      return launchOptions;
    }
  });
  return config;
};

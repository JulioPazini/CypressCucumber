export const LOGIN_INVALIDO = 'Nome de usuário ou senha inválida.';
export const ETIQUETA_CRIADA = 'Etiqueta criada com sucesso';
export const ETIQUETA_EXCLUIDA = 'Etiqueta removida com sucesso';
export const ETIQUETA_SEM_NOME = 'Este campo deve ser preenchido.';
export const ETIQUETA_PESQUISADA_NAO_ENCONTRADA = 'Não existe etiqueta cadastrada com esse nome';
export const ETIQUETA_CANCELAR_CRIACAOEDICAO =
    'Você possui alterações não salvas. Deseja sair e cancelar as alterações?';
export const ETIQUETA_NOME_EXISTENTE = `Etiqueta com descrição -> 'testeautomatizadocriaretiquetas1', já cadastrada para essa lotação!`;
export const DOCUMENTO_NAO_INICIADO = 'Documento não iniciado. Escolha um dos modelos para iniciar um novo documento.';
export const ALTERAR_MODELO_EXPEDIENTE =
    'Ao alterar o modelo você perderá todas as informações inseridas. deseja continuar?';
export const CARD_COM_MAIS_DE_UM_MODELO_VINCULADO =
    'O documento não pode ser iniciado, já existem outros vinculados a esse card de intimação. Por favor, entre em contato com o suporte.';
export const CADASTRO_NAO_REMETDIO = 'Este processo não pode ser remetido, pois não há uma movimentação configurada.';
export const CADASTRO_REMETIDO_SUCESSO = 'Processo remetido com sucesso.';
export const CADASTRO_REMESSA_MESMO_LOCAL =
    'Não é possível remeter um processo para a lotação logada. Selecione outra lotação para realizar a remessa.';
export const CADASTRO_REMESSA_EM_PROCESSAMENTO =
    'Esse processo não pode ser remetido, pois está pendente em outro lote de processamento. Aguarde.';
export const CADASTRO_REMESSA_LOTACAO_NAO_INFORMADA = 'Lotação de destino não informada';
export const CADASTRO_REMESSA_TIPO_CADASTRO_NAO_VINCULADO =
    'Este processo não pode ser remetido, pois a movimentação configurada não está vinculada à este tipo de cadastro.';
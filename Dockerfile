FROM docker-unj-repo.softplan.com.br/unj/react-sdk:erbium

RUN mkdir /app
WORKDIR /app

COPY package.json yarn.lock ./

COPY .npmrc /root/.npmrc

RUN apt-get update
RUN apt-get upgrade -y
RUN apt-get install nginx -y

RUN yarn install --frozen-lockfile

COPY . ./

RUN yarn
